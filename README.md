# 使用说明

基于[bookstack](https://hub.docker.com/r/linuxserver/bookstack)修改，使用[wkhtmltopdf](https://wkhtmltopdf.org/)导出pdf。bookstack默认使用 Dompdf导出pdf，中文字会乱码，根据[官方说明](https://www.bookstackapp.com/docs/admin/pdf-rendering/)在容器内安装[wkhtmltopdf](https://wkhtmltopdf.org/)以及中文字体，环境变量中指定WKHTMLTOPDF就可以导出中文文档了。

## 准备

本地需要安装docker及docker-compose

将git项目克隆到本地

```
git clone https://gitee.com/dreamidea/bookstack-docker-compose.git
```

修改docker-compose.yml，“/home/cherokee/project/docker-bookstack”改成对应你本地的路径。

## 运行
```
docker-compose up -d --build
```
会同时启动bookstack和mysql

## 查看

默认账号名 admin@admin.com 密码为 password, 访问地址 http://dockerhost:6875。

## 关闭
```
docker-compose down
```


## boostack镜像生成
这个步骤不是必须的，修改了Dockfile重新生成镜像的情况下才需要

```
docker build --tag successage/bookstack .
```